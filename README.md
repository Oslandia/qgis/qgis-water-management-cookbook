QGIS water management cookbook
==============================

This page aims to provide recipes to carry out water management task using qgis.

Two plugins for the QGIS processing framework enable to run EPANET and SWMM from qgis.

The qgis-epanet provides additional features to analyse results. An overview article is available [here](http://www.oslandia.com/qgis-plugin-for-water-management-en.html). Several tutorials can help to discover qgis-epanet:
* [a simple network example](simple_network/README.md) also available [in french](http://www.oslandia.com/premiers-pas-avec-qgis-epanet.html)
* [a real life case](aguas_de_coimbra/README.md)

Tips & tricks to create EPANET and SWMM models are listed [here] (tips_tricks/README.md).

Shield: [![CC BY-SA 4.0][cc-by-sa-shield]][cc-by-sa]

This work is licensed under a
[Creative Commons Attribution-ShareAlike 4.0 International License][cc-by-sa].

[![CC BY-SA 4.0][cc-by-sa-image]][cc-by-sa]

[cc-by-sa]: http://creativecommons.org/licenses/by-sa/4.0/
[cc-by-sa-image]: https://licensebuttons.net/l/by-sa/4.0/88x31.png
[cc-by-sa-shield]: https://img.shields.io/badge/License-CC%20BY--SA%204.0-lightgrey.svg
